#include <iostream>
#include <cstdlib>
#include <string>
#include "strvar.h"

void conversation(int max_name_size);
//Continues converstation with user

int main()
{
	using namespace std;
	conversation(30);
	cout << "End of Demo.\n";

	system("pause");

	return 0;
}

//Demo function
void conversation(int max_name_size)
{
	using namespace std;
	using namespace strvarken;

	StringVar your_name(max_name_size), last_name(max_name_size);

	cout << "What is your name(with fnction)?\n";
	your_name.input_line(cin);

	cout << "What is your last name(with cin) ?\n" << last_name ;
	cin >> last_name;

	cout << "We will meet again (firstname + lastname) " << (your_name + last_name) << endl;

	if (your_name == last_name) {
		cout << " FristName = LastName " << endl;
	}
	else {
		cout << " FristName != LastName " << endl;
	}

	cout << "First Aphabet(Before sechar )" << your_name.one_char(0) << endl;

	cout << "Copy Piece(0-3)" << your_name.copy_piece(0,3) << endl;

	your_name.set_char(0, 'A');

	cout << "First Aphabet(After setchar) " << your_name.one_char(0) << endl;
	
	cout << your_name << endl;
}