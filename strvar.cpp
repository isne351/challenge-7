//strvar.cpp
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	// Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))
	{
		value = new char[max_length + 1];

		for (int i = 0; i<strlen(a); i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
	{
		value = new char[max_length + 1];
		for (int i = 0; i<strlen(string_object.value); i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)] = '\0';
	}

	StringVar::~StringVar()
	{
		delete[] value;
	}

	char StringVar::one_char(int index){
		return value[index];
	}

	void StringVar::set_char(int index,char v) {
		value[index]=v;
	}

	int StringVar::get_max() {
		return max_length;
	}

	string StringVar::to_string() {
		string tmp;

		for (int l = 0; l < max_length; l++) {
			tmp = tmp + value[l];
		}

		return tmp;
	}

	string StringVar::copy_piece(int i,int j) {
		string tmp;
		
		for (int l = i; l <= j; l++) {
			tmp = tmp + value[l];
		}

		return tmp;
	}

	string operator +(StringVar f1, StringVar f2) {

		string tmp;

		tmp = f1.to_string() + f2.to_string();

		return tmp;
	}

	bool operator ==(StringVar f1, StringVar f2) {

		if ((f1.to_string() == f2.to_string())) {
			return true;
		}

		return false;

	}

	//Uses cstring
	int StringVar::length() const
	{
		return strlen(value);
	}

	//Uses iostream
	void StringVar::input_line(istream& ins)
	{
		ins.getline(value, max_length + 1);
	}

	istream& operator >>(istream& input, const StringVar& obj) {
		input >> obj.value;
		return input;
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string)
	{
		outs << the_string.value;
		return outs;
	}

}//strvarken
